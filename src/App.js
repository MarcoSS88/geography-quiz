import React, { useState } from "react";
import data from "./data.json";
import QuestionsPanel from "./components/QuestionsPanel";
import AnswersPanel from "./components/AnswersPanel";
import styled from "styled-components";
import foto0 from "./assets/luci-mondo.jpg";
import foto1 from "./assets/mappa-mondo.jpg";
import foto2 from "./assets/mappa-mondo-mano.jpg";
import foto3 from "./assets/tour-eiffeil.jpg";
import foto4 from "./assets/italia-satellite.jpg";
import foto5 from "./assets/altare-della-patria.jpg";

function App() {

  const [pickedCity, setPickedCity] = useState(null)
  const [state, setState] = useState({
    nations: [],
    cities: [],
    gameHasStarted: false,
    counter: 0,
    score: 0,
    backgroundImages: [foto0, foto1, foto2, foto3, foto4, foto5],
  });


  const fetchData = (level) => {
    const nations = data.nations;
    const updatedNations = nations.map((el, index) => {
      return {
        nation: el,
        id: index,
      };
    });

    let mixedNations = [];
    let nationFinal = [];

    while (nationFinal.length < level) {
      // FINCHÈ
      const randomNumber = Math.floor(Math.random() * updatedNations.length);
      const randomNation = updatedNations.find((el) => el.id === randomNumber);
      mixedNations.push(randomNation);
      nationFinal = mixedNations.filter(
        (el, index, array) => index === array.indexOf(el)
      );
    }

    const cities = data.cities.map((el, index) => {
      return {
        cities: el,
        id: index,
      };
    });

    let updatedCities = [];
    for (let nat of nationFinal) {
      updatedCities.push(cities[nat.id]);
    }

    setState({
      ...state,
      cities: updatedCities,
      nations: nationFinal,
      counter: counter + 1,
    });
  };
  const startToPlay = () => setState({ ...state, gameHasStarted: true });

  const pickAnswer = (city) => {
    setPickedCity(city.id);
  };
  const updateScore = () => {
    const { score, counter, cities } = state;
    if (counter <= cities.length) {
      pickedCity === 0
        ? setState({ ...state, score: score + 1, counter: counter + 1 })
        : setState({ ...state, score: score + 0, counter: counter + 1 });
    } else {
      pickedCity === 0
        ? setState({ ...state, score: score + 1 })
        : setState({ ...state, score: score + 0 });
    }
  };
  const refresh = () => {
    setState({
      ...state,
      nations: [],
      cities: [],
      gameHasStarted: false,
      counter: 0,
      cityPicked: null,
      score: 0,
    });
  };

  ///////////////////////////////////////////////////////////////

  const { gameHasStarted, counter, nations, score } = state;

  return (
    <Layout>
      <QuestionsPanel
        startToPlay={startToPlay}
        gameHasStarted={gameHasStarted}
        counter={counter}
        fetchData={fetchData}
        nations={nations}
        score={score}
        updateScore={updateScore}
        refresh={refresh}
      />

      <AnswersPanel state={state} counter={counter} pickAnswer={pickAnswer} />
    </Layout>
  );
}
export default App;

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 100vw;
  height: 100vh;
  background-image: url(${foto0});
  background-size: cover;
`;
