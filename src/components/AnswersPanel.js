import React, { useState, useEffect } from "react";
import styled from "styled-components";

const AnswersPanel = ({ counter, pickAnswer, state }) => {
    const [answers, setAnswers] = useState([]);

    useEffect(() => {
        const { counter, cities } = state;
        const generateAnswers = () => {
            let citiesArray = [];
            let mixedAnswers = [];
            let answersFinal = [];

            if (counter <= cities.length) {
                citiesArray = cities.find((_el, index) => index === counter - 1).cities;
            }

            const citiesWithId = citiesArray.map((el, index) => {
                return {
                    city: el,
                    id: index,
                    isSelected: false,
                };
            });

            while (answersFinal.length < 4) {
                const randomNumber = Math.floor(Math.random() * citiesWithId.length);
                const city = citiesWithId[randomNumber];
                mixedAnswers.push(city);
                answersFinal = mixedAnswers.filter(
                    (el, index, array) => index === array.indexOf(el)
                );
            }
            setAnswers(answersFinal);
        };
        if (state.cities.length && counter <= cities.length) {
            state.cities.length && generateAnswers();
        }
    }, [state.counter]);

    const displayAnswers = () => {
        return answers.map((el, index) => {
            return (
                <Answer onClick={() => pickAnswer(el)} key={index}>
                    {el.city}
                </Answer>
            );
        });
    };

    return <Layout show={counter}>{answers ? displayAnswers() : null}</Layout>;
};
export default AnswersPanel;

const Layout = styled.div`
  display: ${({ show }) => (show > 0 ? "grid" : "none")};
  grid-template-columns: auto auto;
  grid-template-rows: auto auto;
  align-content: space-around;
  justify-content: space-around;
  width: 60vw;
  height: 35vh;
  border: 1px solid var(--paleSilver);
  border-radius: 1rem;
  background-color: var(--elettricBlue);

  @media (max-width: 670px) {
    width: 55rem;
  }
`;
const Answer = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 25rem;
  height: 6rem;
  border: none;
  outline: none;
  border-radius: 1.5rem;
  background-color: var(--darkPurple1);
  color: var(--paleSilver);
  font-family: "Rajdhani", sans-serif;
  font-size: 2.2rem;
  font-weight: 500;
  letter-spacing: 1.5px;

  &:hover {
    cursor: pointer;
    background-color: var(--darkPurple2);
  }
`;
