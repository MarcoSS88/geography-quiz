import React from 'react';
import styled from 'styled-components';

const Level = ({ fetchData, gameHasStarted, counter }) => {

    const showHide = () => {
        if (gameHasStarted && counter === 0) {
            return 'flex';
        } else return 'none';
    }

    return (
        <Layout style={{ display: showHide() }}>
            <Easy onClick={() => fetchData(10)}><span>Easy</span></Easy>
            <Intermediate onClick={() => fetchData(20)}><span>Intermediate</span></Intermediate>
            <Hard onClick={() => fetchData(30)}><span>Hard</span></Hard>
        </Layout>
    )
}
export default Level;

const Layout = styled.div`
    align-items: flex-start;
    justify-content: space-around;
    width: 100%;
    height: 40%;

    @media (max-width: 670px) {
        display: ${({ show }) => show ? 'flex' : 'none'};
        flex-wrap: wrap;
        align-content: space-between;
        height: 60%;
    }
`
const Easy = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 17rem;
    height: 4.5rem;
    border: none;
    outline: none;
    border-radius: 1rem;
    background-color: var(--rustyRed);
    color: var(--paleSilver);
    font-family: 'Rajdhani', sans-serif;
    font-size: 2.2rem;
    font-weight: 500;
    letter-spacing: 1.5px;

    &:hover {
        cursor: pointer;
        filter: brightness(115%);
    }

    &:hover span{
        display: none;
    }

    &:hover:before {
        content: '10 Questions';
    }

    @media (max-width: 670px){
        height: 3.5rem;
    }
`
const Intermediate = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 17rem;
    height: 4.5rem;
    border: none;
    outline: none;
    border-radius: 1rem;
    background-color: var(--rustyRed);
    color: var(--paleSilver);
    font-family: 'Rajdhani', sans-serif;
    font-size: 2.2rem;
    font-weight: 500;
    letter-spacing: 1px;

    &:hover {
        cursor: pointer;
        filter: brightness(115%);
    }

    &:hover span{
        display: none;
    }

    &:hover:before {
        content: '20 Questions';
    }

    @media (max-width: 670px){
        height: 3.5rem;
    }
`
const Hard = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 17rem;
    height: 4.5rem;
    border: none;
    outline: none;
    border-radius: 1rem;
    background-color: var(--rustyRed);
    color: var(--paleSilver);
    font-family: 'Rajdhani', sans-serif;
    font-size: 2.2rem;
    font-weight: 500;
    letter-spacing: 1px;

    &:hover {
        cursor: pointer;
        filter: brightness(115%);
    }

    &:hover span{
        display: none;
    }

    &:hover:before {
        content: '30 Questions';
    }

    @media (max-width: 670px){
        height: 3.5rem;
        margin-bottom: 2%;
    }
`

