import React from "react";
import styled from "styled-components";
import Level from "./Level";

const QuestionsPanel = ({ startToPlay, gameHasStarted, counter, fetchData, nations, score, updateScore, refresh }) => {
  const changeText1 = () => {
    if (counter > nations.length) {
      return `You finish the quiz!`;
    } else return `Question num ${counter}`;
  };
  const changeText2 = () => {
    if (!gameHasStarted) {
      return "Test your geography knowledge!";
    } else if (gameHasStarted && counter === 0) {
      return "Chose your level:";
    } else if (counter >= 1 && counter <= nations.length) {
      return `What's the capital of ${nations[counter - 1].nation} ?`;
    } else if (counter > nations.length) {
      return `Your score is: ${score}/${nations.length}`;
    }
  };
  const showHide1 = () => {
    return counter >= 1 && counter <= nations.length ? "flex" : "none";
  };
  const showHide2 = () => {
    return counter > nations.length ? "flex" : "none";
  };

  return (
    <Layout>
      <TopDiv>
        <Text1 show={counter}>{changeText1()}</Text1>
        <Text2>{changeText2()}</Text2>
      </TopDiv>

      <BottomDiv>
        <StartButton show={gameHasStarted} onClick={() => startToPlay()}>
          <span className="material-icons">public</span>I wanna play !
        </StartButton>
        <NextButton
          style={{ display: showHide1() }}
          onClick={() => updateScore()}
        >
          Next
        </NextButton>
        <StartAgainBtn
          style={{ display: showHide2() }}
          onClick={() => refresh()}
        >
          Play again!
        </StartAgainBtn>
      </BottomDiv>

      <Level
        gameHasStarted={gameHasStarted}
        counter={counter}
        fetchData={fetchData}
      />
    </Layout>
  );
};
export default QuestionsPanel;

const Layout = styled.div`
  display: flex;
  flex-direction: column;

  width: 45vw;
  height: 35vh;
  border: 1px solid var(--paleSilver);
  border-radius: 1rem;
  background-color: var(--elettricBlue);
  @media (max-width: 670px) {
    width: 40rem;
  }
`;
const TopDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  height: 60%;

  @media (max-width: 670px) {
  }
`;
const BottomDiv = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  width: 100%;
  height: 40%;
`;
const Text1 = styled.p`
  display: ${({ show }) => (show > 0 ? "flex" : "none")};
  font-size: 3rem;
  font-weight: 600;
  letter-spacing: 1px;
  color: var(--darkPurple2);

  @media (max-width: 670px) {
    font-size: 2.5rem;
    font-weight: 500;
  }
`;
const Text2 = styled.p`
  font-size: 3rem;
  font-weight: 600;
  letter-spacing: 1px;
  color: var(--paleYellow);

  @media (max-width: 670px) {
    font-size: 2.5rem;
    font-weight: 500;
  }
`;
const StartButton = styled.button`
  display: ${({ show }) => (show ? "none" : "flex")};
  align-items: center;
  justify-content: space-around;
  width: 25rem;
  height: 5.5rem;
  border: none;
  outline: none;
  border-radius: 1.5rem;
  background-color: var(--rustyRed);
  color: var(--paleSilver);
  font-family: "Rajdhani", sans-serif;
  font-size: 2.6rem;
  font-weight: 550;
  letter-spacing: 2px;

  & > span {
    zoom: 1.3;
  }

  &:hover {
    cursor: pointer;
    filter: brightness(115%);
  }
`;
const NextButton = styled.button`
  align-items: center;
  justify-content: space-around;
  width: 20rem;
  height: 4.5rem;
  border: none;
  outline: none;
  border-radius: 1.5rem;
  background-color: var(--roseDust);
  color: var(--paleSilver);
  font-family: "Rajdhani", sans-serif;
  font-size: 2.8rem;
  font-weight: 550;
  letter-spacing: 5px;

  &:hover {
    cursor: pointer;
    filter: brightness(105%);
  }
`;
const StartAgainBtn = styled.button`
  align-items: center;
  justify-content: space-around;
  width: 20rem;
  height: 4.5rem;
  border: none;
  outline: none;
  border-radius: 1.5rem;
  background-color: var(--roseDust);
  color: var(--paleSilver);
  font-family: "Rajdhani", sans-serif;
  font-size: 2.8rem;
  font-weight: 550;
  letter-spacing: 5px;

  &:hover {
    cursor: pointer;
    filter: brightness(105%);
  }
`;
