import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
* {
    padding:0;
    margin: 0;
    box-sizing: border-box;
}

html {
    font-family: 'Rajdhani', sans-serif;
    font-size: 62.5%;
    text-rendering: optimizeLegibility;
}

:root{
    --darkPurple1: #240115;
    --rustyRed: #BC4B51;
    --roseDust: #BC4B51;
    --elettricBlue: rgb(135, 245, 251, 0.3);
    --darkPurple2: #2F131E;
    --paleSilver: #CEC3C1;
    --paleYellow: #FDFFB6;
}

`
export default GlobalStyle;
